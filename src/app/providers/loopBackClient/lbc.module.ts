import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { LoopbackClient } from './lbc.client';
import { CookieService } from 'ngx-cookie-service';


@NgModule({
    imports: [HttpClientModule],
    exports: [],
    providers: [LoopbackClient,CookieService]
})
export class LbcModule {}
