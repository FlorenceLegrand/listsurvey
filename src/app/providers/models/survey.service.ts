import { Injectable } from '@angular/core';

import { ModelService } from './model.service'

@Injectable()
export class SurveyService extends ModelService {

    constructor() {
        super('Survey');
    }

}