// component pour intégrer le JSONeditor (éditer les formulaires)

import { Component, OnInit, ViewChild } from '@angular/core';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';

import { SurveyEditorService } from '../survey-editor.service';
import { Spinkit } from 'ng-http-loader/spinkits';

@Component({
  selector: 'survey-editor',
  templateUrl: './survey-editor.component.html',
  styleUrls: ['./survey-editor.component.css']
})
export class SurveyEditorComponent implements OnInit {

  public editorOptions: JsonEditorOptions;
  dataSurveyModel: any = []; //récup du service
  elemData: any = {}; //pour l'affichage - lien avec le html

  public spinkit = Spinkit; //loader

  @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;

  constructor(
    public surveyEditorService: SurveyEditorService
  ) {
    this.editorOptions = new JsonEditorOptions()
  }

  ngOnInit() {
    this.getDataService();
  }

  async getDataService() { //récup des models existants via le service
    this.dataSurveyModel = await this.surveyEditorService.getDataS();
    this.editorOptions.modes = ['code', 'text', 'tree'];
    await this.editor.setOptions(this.editorOptions);
  }

  setDataService(elem) { //récup du contenu pour affichage dans l'editor
    this.elemData = elem;
  }

  async updateModelEditor(elemData) {
    const changedJson = await this.editor.get(); //récup des modifs du json dans l'editor
    if (elemData['model'] === changedJson['model']) { //update
      await this.surveyEditorService.updateModel(changedJson);  // envoi des modifs vers le service
      await this.getDataService(); //récupération de la MAJ à l'affichage
    }
    else {
      await this.surveyEditorService.createModel(changedJson);  // create
      await this.getDataService(); //récupération de la vue à l'affichage
    }
  }

}


