import { TestBed, inject } from '@angular/core/testing';

import { SurveyEditorService } from './survey-editor.service';

describe('SurveyEditorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SurveyEditorService]
    });
  });

  it('should be created', inject([SurveyEditorService], (service: SurveyEditorService) => {
    expect(service).toBeTruthy();
  }));
});
