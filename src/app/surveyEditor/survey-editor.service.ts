import { Injectable } from '@angular/core';
import { surveyModelService } from '../providers/models/surveyModel.service';

@Injectable()
export class SurveyEditorService {

  constructor(private surveyModelService: surveyModelService) { }


  getDataS() { //récup des models existants
    return new Promise(async (resolve, reject) => {
      try {
        let dataSurveyModel = await this.surveyModelService.find({})
        resolve(dataSurveyModel)
      }
      catch (err) {
        reject(err)
      }
    })
  }

  createModel(model) {
    return new Promise(async (resolve, reject) => {
      try {
        delete model.id; //supprimer l'id du model pour pouvoir en créer un nouveau
        await this.surveyModelService.create(model)
        resolve();
      }
      catch (err) {
        reject(err)
      }
    })
  }

  updateModel(model) { //envoi vers la BDD du model modifié
    return new Promise(async (resolve, reject) => {
      try {
        await this.surveyModelService.updateById(model.id, model)
        resolve();
      }
      catch (err) {
        reject(err)
      }
    })
  }

}