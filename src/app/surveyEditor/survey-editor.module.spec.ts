import { SurveyEditorModule } from './survey-editor.module';

describe('SurveyEditorModule', () => {
  let surveyEditorModule: SurveyEditorModule;

  beforeEach(() => {
    surveyEditorModule = new SurveyEditorModule();
  });

  it('should create an instance', () => {
    expect(surveyEditorModule).toBeTruthy();
  });
});
