import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyChoiceModalComponent } from './survey-choice-modal.component';

describe('SurveyChoiceModalComponent', () => {
  let component: SurveyChoiceModalComponent;
  let fixture: ComponentFixture<SurveyChoiceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyChoiceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyChoiceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
