// //modal pour proposer le choix du questionnaire 
// utilise ngbModal --> le déclarer dans entryComponent du NgModule

import { Component, OnInit, EventEmitter, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Spinkit } from 'ng-http-loader/spinkits';

import { SurveysService } from '../surveys.service';

@Component({
  selector: 'survey-choice-modal',
  templateUrl: './survey-choice-modal.component.html',
  styleUrls: ['./survey-choice-modal.component.css']
})
export class SurveyChoiceModalComponent implements OnInit {
  @Input() models: any; //vient de surveyComponent (modèles récupérés au result du Get dans surveyComponent). utilisé dans le HTML
  public spinkit = Spinkit; //loader

  constructor(public activeModal: NgbActiveModal, private surveysService: SurveysService) { }

  ngOnInit() {
  }

  selectChoice(elemChoice) { //lien avec le HTML
    this.activeModal.close(elemChoice) //renvoi le choix sélectionné au surveyComponent (close ce modal et ouvre le suivant avec cette info)
  }
}
