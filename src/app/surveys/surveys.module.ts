import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgHttpLoaderModule } from 'ng-http-loader/ng-http-loader.module';
import { HttpClientModule } from '@angular/common/http';

import { SurveyModalComponent } from './survey-modal/survey-modal.component';
import { SurveyComponent } from './survey/survey.component';
import { SurveyChoiceModalComponent } from './survey-choice-modal/survey-choice-modal.component';
import { SurveysService } from './surveys.service';


@NgModule({
  imports: [
    CommonModule,
    NgHttpLoaderModule, //pour loader spinkit
    HttpClientModule
  ],
  declarations: [SurveyComponent, SurveyModalComponent, SurveyChoiceModalComponent],
  exports: [SurveyComponent,],
  providers: [SurveysService],
  entryComponents : [SurveyModalComponent,SurveyChoiceModalComponent]
})
export class SurveysModule { }
