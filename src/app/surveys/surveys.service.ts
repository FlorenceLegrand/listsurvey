import { Injectable } from '@angular/core';
import { MultipleTextItem } from 'survey-angular';

import { SurveyService } from '../providers/models/survey.service';
import { surveyModelService } from '../providers/models/surveyModel.service';

@Injectable()
export class SurveysService {

  listName = "";
  surveyList: any[] = []; //liste / tableau de tous les questionnaires survey
  surveyForm: any = {}

  constructor(private surveyService: SurveyService,
    private surveyModelService: surveyModelService) { }


  getSurvey(param?) { //va récupérer la structure des models dans la BDD (collection SurveyModel via surveyModelService dans providers)
    return new Promise(async (resolve, reject) => {
      try {
        if (param) {
          let modelParam = param['model']
          this.surveyForm = await this.surveyModelService.find({ filter: { where: { model: `${modelParam}` } } })
          resolve(this.surveyForm[0])
        }
        else {
          this.surveyForm = await this.surveyModelService.find({})
          resolve(this.surveyForm)
        }
      }
      catch (err) {
        reject(err)
      }
    })
  }

  getListSurvey() { //va récupérer le contenu des formulaires dans la BDD (collection Survey via surveyService dans providers)
    return new Promise(async (resolve, reject) => {
      try {
        let listSurvey = await this.surveyService.find({})
        if (listSurvey) {
          resolve(listSurvey)
        }
        else {
          resolve(null)
        }
      }
      catch (err) {
        reject(err)
      }
    })
  }

  //creation d'un questionnaire
  addSurvey(paramResSurvey: Object) { //res correspond à resSurvey du survey.ts
    return new Promise(async (resolve, reject) => {
      try {
        await this.surveyService.create(paramResSurvey);
        resolve();
        let name = paramResSurvey['nomForm'];
        alert(`Le questionnaire "${name}" a bien été enregistré !`)
      }
      catch (err) {
        reject(err)
      }
    })
  }

  //mise a jour d'un questionnaire(si nomForm est modifé ->addSurvey)
  updateSurvey(paramResSurvey) {
    return new Promise(async (resolve, reject) => {
      try {
        let name = paramResSurvey['nomForm']
        let lists = await this.surveyService.find({ filter: { where: { nomForm: `${name}` } } })
        await this.surveyService.updateById(lists[0].id, paramResSurvey)
        resolve();
        alert(`Le questionnaire "${name}" a bien été mis à jour !`)
      }
      catch (err) {
        reject(err)
      }
    })
  }

  //supprime un questionnaire dans la liste
  deleteSurvey(idDEparamListSurvey) {
    return new Promise(async (resolve, reject) => {
      let confirmRes = confirm(`Etes-vous sûr de vouloir supprimer ce questionnaire ?`);
      if (confirmRes === true) {
        try {
          await this.surveyService.deleteById(idDEparamListSurvey);
          resolve();
        }
        catch (err) {
          reject(err)
        }
      }
    })
  }
}
