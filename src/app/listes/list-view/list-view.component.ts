// // component NgBModal --> affiche une liste (create ou update)
// - déclarer le component dans entryComponent du NgModule
// - importer NgBModal dans le composant pour pouvoir l'utiliser dans le constructeur

import { Component, Input, Renderer2, OnInit, NgModule } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { ListesService } from '../listes.service';
import { List, Options } from '../data-model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnInit {
  @Input() list: List; //import de listComp
  title: string;
  listForm: FormGroup;
  emptyOption: Options;

  constructor(private fb: FormBuilder,
    public activeModal: NgbActiveModal,
    private renderer: Renderer2) {    //import renderer2 pour créer le focus
    this.createForm();
  }

  ngOnInit() {

    if (this.list && (typeof this.list.id !== 'undefined')) {
      this.title = "Modification d'une liste"
    }
    else {
      this.list =
        { //initialisation d'une liste vide pour "créer une liste"
          listName: '',
          comment: '',
          options: [{
            key: '',
            optionName: ''
          }]
        };
      this.title = 'Ajouter une liste'
    }

    let inputElement = this.renderer.selectRootElement('#focusMe');//recuperation de input grace a id focusMe 
    inputElement.focus();//appel de la fonction focus sur l'input
  }

  createForm() { //création du form vide
    this.listForm = this.fb.group({
      listName: '',
      comment: '',
      optionName: this.fb.array([])
    });
  }


  addOption() { // appelé au bouton "ajouter une option"
    this.emptyOption = {
      key: '',
      optionName: ''
    }
    this.list.options.push({
      key: this.emptyOption.key,
      optionName: this.emptyOption.optionName
    })
  }

  deleteOption(i) {
    let confirmRes = confirm('Etes-vous sûr de vouloir supprimer cette option ?');
    if (confirmRes === true) {
      this.list.options.splice(i, 1);
    }
  }

  onSubmit() {
    this.activeModal.close(this.list); // renvoie la liste (créée ou modifiée) à list-Comp dans .open Modal (promise)
  }

}


