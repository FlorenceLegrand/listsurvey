import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ListesModule } from './listes/listes.module';
import { ListViewComponent } from './listes/list-view/list-view.component';
import { SurveysModule } from './surveys/surveys.module';
import { ModelsModule } from './providers/models/models.module';
import { SurveyEditorModule } from './surveyEditor/survey-editor.module';

@NgModule({
  imports: [
    BrowserModule,
    ListesModule,
    SurveysModule,
    SurveyEditorModule,
    ModelsModule,
    NgbModule.forRoot(),

  ],
  declarations: [
    AppComponent,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }

