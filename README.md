
# To run the app :
- clone the project + npm install
- create a database (mongoDB): démarrer MongoDB 
(windows : "C:\ProgramFile....\mongod.exe" + --dbpath "chemin vers fichier data"  / mac : terminal dans dossier mongodb: ./start/sh) 
- clone the api (+ npm install) or create an API Loopback / module provider :
création des collections ici nommées : List, Survey, SurveyModel
En parallèle création dans ListSurvey de 3 fichiers dans providers/models : list.service, survey.service, surveyModel.service qui font le lien avec les collections de la base 
commande de démarrage: node . (localhost:3000/explorer)

- lancer l'appli angular : ng serve (localhost:4200)

#npm install effectués au cours du développement 
npm install (pour nodes_modules)
npm install @ng-bootstrap/ng-bootstrap (pour ngbModal)
(pour survey)
npm install survey-angular
npm install surveyjs-widgets
npm install image-picker

npm install knockout !!! ajouter « knockout » dans types dans le config.json

# image-picker
npm install --> installe dans le dossier node_modules.
pour l'installer dans le webpack : dans le dossier node_modules/image-picker/image-picker récupérer les chemins des fichiers .CSS et .JS pour les mettre dans angular-cli.json (style : CSS / script : JS) puis enlever les liens <script></script> s'ils existent dans l'index.html (ou autre html...-)
imagepicker renvoie à un élément "imagepicker" dans le json/formulaire 

# NgBModal
pour les modal, penser à ajouter le component dans "entryComponent" du module
+ import de NgbModal dans le constructeur

# vjqqlndmdag.angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
